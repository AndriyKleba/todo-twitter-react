import React, {Component} from "react";
import './post-add-form.css';

export default class PostAddForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            text: '',
        };
        this.onInputChange = this.onInputChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onInputChange(event) {
        this.setState({
            text: event.target.value
        })
    }

    onSubmit(e) {
        e.preventDefault();
        this.props.onAddItem(this.state.text);
        this.setState({
            text: ''
        })

    }

    render() {

        return (
            <form
                onSubmit={this.onSubmit}
                className="bottom-panel d-flex"
            >
                <input
                    className="form-control new-post-label"
                    type="text"
                    placeholder="Про що ви думаєте зараз?"
                    onChange={this.onInputChange}
                    value={this.state.text}
                />
                <button
                    type="submit"
                    className="btn btn-outline-secondary"
                >
                    Добавити
                </button>
            </form>
        )
    }

};

